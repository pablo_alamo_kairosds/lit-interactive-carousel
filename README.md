<h1 align="center">genk-interactive-carousel</h1>

Interactive carousel built with Lit-Element. This webcomponent follows the [open-wc](https://github.com/open-wc/open-wc) recommendation.

<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#installation">Installation</a>
    </li>
    <li>
      <a href="#built-with">Built With</a>
    </li>
    <li>
      <a href="#usage">Usage</a>
    </li>
    <li>
      <a href="#properties">Properties</a>
    </li>
    <li>
      <a href="#events">Events</a>
    </li>
    <li>
      <a href="#linting">Linting</a>
    </li>
    <li>
      <a href="#testing">Testing</a>
    </li>
    <li>
      <a href="#tooling-configs">Tooling configs</a>
    </li>
    <li>
      <a href="#local-demo">Local Demo</a>
    </li>
    <li>
      <a href="#contact">Contact</a>
    </li>
  </ol>
</details>

## Installation

```bash
npm install genk-interactive-carousel --save
```

## Built With

* [Node.js](https://nodejs.org/en/) (v18.0.0)
* [Lit](https://lit.dev/)

## Usage

```html
<script type="module">
  import 'genk-interactive-carousel/genk-interactive-carousel.js';
</script>

<genk-interactive-carousel>
  <p>First slide</p>
  <p>Second slide</p>
  <p>Third slide</p>
  <p>Fourth slide</p>
</genk-interactive-carousel>
```

## Properties

- **slideIndex**: Number that indicates the current active slide
- **animations**: Object with the animations in [Web Animation API](https://developer.mozilla.org/en-US/docs/Web/API/Web_Animations_API) Format, see src/animations.js for more details.

```javascript
const SLIDE_LEAVE = [
  SLIDE_LEAVE_KEYFRAMES,
  SLIDE_LEAVE_OPTS
];
const SLIDE_ENTER = [
  SLIDE_ENTER_KEYFRAMES,
  SLIDE_ENTER_OPTS
];

const ANIMATIONS = {
  slideEnter: SLIDE_ENTER,
  slideLeave: SLIDE_LEAVE
};
```

## Events

- **genk-ic-next**: dispatched when slide change to next one ➡️
- **genk-ic-prev**: dispatched when slide change to previous one ⬅️

## Linting

To scan the project for linting and formatting errors, run

```bash
npm run lint
```

To automatically fix linting and formatting errors, run

```bash
npm run format
```

## Testing

To execute a single test run:

```bash
npm run test
```

To run the tests in interactive watch mode run:

```bash
npm run test:watch
```


## Tooling configs

For most of the tools, the configuration is in the `package.json` to minimize the amount of files in your project.

If you customize the configuration a lot, you can consider moving them to individual files.

## Local Demo

```bash
npm run start
```

## Contact

Pablo Álamo - [LinkedIn](https://www.linkedin.com/in/pablodevs/) - pabloalamovargas@gmail.com

<div align="right">
  <p>(<a href="#top">back to top</a>)</p>
</div>
