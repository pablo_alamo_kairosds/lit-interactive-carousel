import { html, LitElement } from 'lit';
import { styleMap } from 'lit/directives/style-map.js';
import { genkInteractiveCarouselStyles } from './genk-interactive-carousel-style.js';

/**
 * `genk-interactive-carousel`
 * GenkInteractiveCarousel
 *
 * @customElement genk-interactive-carousel
 * @litElement
 * @demo demo/index.html
 */
export class GenkInteractiveCarousel extends LitElement {
  static get is() {
    return 'genk-interactive-carousel';
  }

  static get styles() {
    return [genkInteractiveCarouselStyles];
  }

  static get properties() {
    return {
      /**
       * Current active slide
       * @property
       * @type { Number }
       */
      slideIndex: {
        type: Number,
      },
      /**
       * Carousel Height
       * @property
       * @type { Number }
       */
      _containerHeight: {
        type: Number,
        state: true,
      },
      /**
       * Animations Object
       * @property
       * @type { Object }
       */
      animations: {
        type: Object,
      },
      /**
       * Total number of slides
       * @property
       * @type { Number }
       */
       numOfSlides: {
        type: Number,
        state: true
      },
      /**
       * Array of slides
       * @property
       * @type { Array }
       */
       slides: {
        type: Array,
        state: true
      },
    };
  }

  attributeChangedCallback(name, oldval, newval) {
    this._navigateToSlide(newval, oldval)
    // eslint-disable-next-line wc/guard-super-call
    super.attributeChangedCallback(name, oldval, newval);
  }

  async firstUpdated() {
    this.slides = [...this.children];
    this.numOfSlides = this.slides.length;
    
    await this.updateComplete;
    this.slides.forEach(slide => {
      this.hideSlide(slide);
    })
    this.showSlide(this.slides[this.slideIndex]);
    this._containerHeight = this._getCarouselHeight();
  }

  _getCarouselHeight() {
    return Math.max(0, ...this.slides.map(slide => slide.offsetHeight));
  }

  // eslint-disable-next-line class-methods-use-this
  hideSlide(slide) {
    slide.classList.add('hidden-slide');
  }

  // eslint-disable-next-line class-methods-use-this
  showSlide(slide) {
    slide.classList.remove('hidden-slide');
    slide.animate(...this.animations.slideEnter);
  }
  
  async _navigateToSlide(nextSlideIndex, prevSlideIndex) {
    if (!this.numOfSlides || nextSlideIndex < 0 || nextSlideIndex === this.numOfSlides) {
      return;
    }

    if (nextSlideIndex > prevSlideIndex) {
      this.dispatchEvent(new Event('genk-ic-next', { bubbles: true, composed: true }));
    } else {
      this.dispatchEvent(new Event('genk-ic-prev', { bubbles: true, composed: true }));
    }

    const leavingSlide = this.slides[this.slideIndex];
    const leavingAnimation = leavingSlide.animate(...this.animations.slideLeave);

    this.slideIndex = nextSlideIndex;

    const enteringSlide = this.slides[this.slideIndex];
    this.showSlide(enteringSlide);

    await leavingAnimation.finished;
    this.hideSlide(leavingSlide);
  }

  render() {
    const containerStyles = {
      height: `${this._containerHeight}px`,
    };

    return html` <div
      part="container"
      class="container"
      style="${styleMap(containerStyles)}"
    >
      <slot></slot>
    </div>`;
  }
}
