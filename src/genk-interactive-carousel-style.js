import { css } from 'lit';

export const genkInteractiveCarouselStyles = css`
  :host {
    display: block;
    width: 100%;
  }
  
  .container {
    position: relative;
    display: flex;
    align-items: center;
    justify-content: center;
    width: 100%;
  }

  ::slotted(*) {
    position: absolute;
    display: block;
    opacity: 0;
  }

  ::slotted(.hidden-slide) {
    display: none;
  }
`;
