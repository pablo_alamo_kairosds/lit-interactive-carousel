/* ========== CAROUSEL ANIMATIONS ========== */
const SLIDE_LEAVE_KEYFRAMES = [
  { opacity: 1 },
  { opacity: 0 }
];
const SLIDE_ENTER_KEYFRAMES = [
  {
    transform: 'translateX(-1rem)',
    opacity: 0
  },
  {
    transform: 'translateX(0)',
    opacity: 1
  }
];

const SLIDE_LEAVE_OPTS = {
  duration: 300,
  easing: 'ease-in',
  iterations: 1,
  delay: 150,
  fill: 'forwards'
};
const SLIDE_ENTER_OPTS = {
  duration: 400,
  easing: 'ease-in',
  iterations: 1,
  delay: 600,
  fill: 'forwards'
};

const SLIDE_LEAVE = [
  SLIDE_LEAVE_KEYFRAMES,
  SLIDE_LEAVE_OPTS
];
const SLIDE_ENTER = [
  SLIDE_ENTER_KEYFRAMES,
  SLIDE_ENTER_OPTS
];

export const ANIMATIONS = {
  slideEnter: SLIDE_ENTER,
  slideLeave: SLIDE_LEAVE
};